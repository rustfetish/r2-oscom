// Rust system commands

//Yansi Package
use ::yansi::{Color, Paint, Style};

fn main() {
    println!("Hello, world!");
    col_pr();
}

fn col_pr() {
    // Writting the yansi stuff here first

    // Asked byt he compiler
    #[derive(Debug)]
    // Defining the struct
    struct S1 {
        s1: String,
        s2: String,
    }

    let p1 = S1 {
        s1: "TF u looking at huh ?".to_string(),
        s2: "yes Dumb Dumb".to_string(),
    };

    println!("{:?}", p1);
    println!("{:?}", Paint::magenta(p1));
}
