// Importing elements from the yansi pckage



/*
These are for suppressing errors 
*/

#[warn(unused_variables)]


fn main() {
    println!(
        "
    Testing out yansi color printing
    "
    );
    luptest();
}

/*
Loop test -
source - https://doc.rust-lang.org/rust-by-example/flow_control/for.html
- Multiple Methods
*/
fn luptest() {
    for n in 1..5 {
        println!("Basket-Loop");
    }
}
